require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
const port = process.env.PORT || 3000;
const URL_BASE = '/techu/v2/';
const URL_mLab = 'https://api.mlab.com/api/1/databases/techu21db/collections/';
const apikey_mlab = 'apiKey=' + process.env.API_KEY_MLAB;

app.listen(port, function(){
  console.log('NodeJS port: ', port);
});

app.use(body_parser.json());

app.get('/', function(req, req){
  req.send("Hola API xy");
});

app.get(URL_BASE + 'users', function(request, response){
  const http_client = request_json.createClient(URL_mLab);
  const fieldParam = 'f={"_id":0}&';
  http_client.get('user?' + fieldParam + apikey_mlab, function(err, res, body){
    var tempResponse = {};
    if(err) {
      tempResponse = {"msg" : "Error al recuperar users de mLab."}
      response.status(500);
    } else {
      if(body.length > 0) {
        tempResponse = body;
      } else {
        tempResponse = {"msg" : "Usuario no encontrado."};
        response.status(404);
      }
    }
    response.send(tempResponse);
  });
});

app.get(URL_BASE + 'users/:id', function(request, response){
  let id = request.params.id;
  const http_client = request_json.createClient(URL_mLab);
  let queryString = `q={"id_user":${id}}&`;
  const fieldParam = 'f={"_id":0,"account":0}&';
  http_client.get('user?' + queryString + fieldParam + apikey_mlab, function(err, res, body){
    var tempResponse = {};
    if(err) {
      tempResponse = {"msg" : "Error al recuperar users de mLab."}
      response.status(500);
    } else {
      if(body.length > 0) {
        tempResponse = body;
      } else {
        tempResponse = {"msg" : "Usuario no encontrado."};
        response.status(404);
      }
    }
    response.send(tempResponse);
  });
});

app.get(URL_BASE + 'users/:id/accounts', function(request, response){
  let id = request.params.id;
  const http_client = request_json.createClient(URL_mLab);
  let queryString = `q={"id_user":${id}}&`;
  const fieldParam = 'f={"_id":0, "account":1}&';
  http_client.get('user?' + queryString + fieldParam + apikey_mlab, function(err, res, body){
    var tempResponse = {};
    if(err) {
      tempResponse = {"msg" : "Error al recuperar cuentas."}
      response.status(500);
    } else {
      if(body.length > 0) {
        tempResponse = body;
      } else {
        tempResponse = {"msg" : "No tiene cuentas"};
        response.status(404);
      }
    }
    response.send(tempResponse);
  });
});

app.post(URL_BASE + 'users', function(request, response){
  let id = request.params.id;
  const http_client = request_json.createClient(URL_mLab);
  let queryString = 'c=true&';
  http_client.get('user?' + queryString + apikey_mlab, function(err, res, total){
    let newID = total + 1;
    console.log(newID);
    var newUser = {
      "id_user" : newID,
      "first_name" : request.body.first_name,
      "last_name" : request.body.last_name,
      "email" : request.body.email,
      "password" : request.body.password
    };
    console.log(newUser);
    http_client.post(URL_mLab + "user?" + apikey_mlab, newUser, function(error, respuestaMLab, bodyPost) {
      response.send(bodyPost);
    });
  });
});

app.put(URL_BASE + 'users/:id', function (request, response) {
  console.log("PUT /techu/v1/users/:id");
  let idBuscar = request.params.id;
  delete request.body.id_user;
  console.log(request.body);
  let queryStringID=`q={"id_user":${idBuscar}}&`;
  let cambio = '{"$set":' + JSON.stringify(request.body) + '}';
  let http_client = request_json.createClient(URL_mLab);
  http_client.put('user?' + queryStringID + apikey_mlab, JSON.parse(cambio), 
  function(err, respuestaMLab, body) {
    var tempResponse = {};
    if(err) {
      tempResponse = {"msg" : "Error al actualizar."}
      response.status(500);
    } else {
      console.log(body);
      console.log(body.n);
      if(body.n > 0) {
        //tempResponse = body;
        tempResponse = {"msg" : `Actualizo Correctamente id: ${idBuscar}`};
      } else {
        tempResponse = {"msg" : "No se encontro usuario"};
        response.status(404);
      }
    }
    response.send(tempResponse);
  });
});

app.delete(URL_BASE + 'users/:id', function(request, response){
  console.log("Ingresa");
  let idBuscar = request.params.id;
  let queryStringID=`q={"id_user":${idBuscar}}&`;
  let http_client = request_json.createClient(URL_mLab);
  http_client.get('user?' + queryStringID + apikey_mlab, function(err, respuesta, body) {
    let tempResponse = {};
    if(err) {
      tempResponse = {"msg" : "Error al recuperar users de mLab."}
      response.status(500);
    } else {
      if(body.length > 0) {
        tempResponse = body;
        console.log(body);
        http_client.delete("user/"+ body[0]._id.$oid +'?'+ apikey_mlab, 
          function(errorD, respuestaMLabD, bodyD){
            if(errorD) {
              tempResponse = {"msg" : "Error al eliminar."}
              response.status(500);
            } else {
              tempResponse = bodyD;
            }
            response.send(tempResponse);
        });
      } else {
        tempResponse = {"msg" : "Usuario no encontrado."};
        response.status(404);
        response.send(tempResponse);
      }
    }
  });
});

app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /apicol/v2/login");
    console.log(request.body.email);
    console.log(request.body.password);

    let user = request.body.email;
    let pass = request.body.password;

    let queryStringEmail='q={"email":"' + user + '"}&';

    let http_client = request_json.createClient(URL_mLab);
    http_client.get('user?' + queryStringEmail + apikey_mlab, function(err, rpt, body) {
      console.info(body);
      let tempResponse = {};
      var respuesta = body[0];
      if(err) {
        tempResponse = {"msg" : "Error al recuperar users de mLab."}
        response.status(500);
        response.send(tempResponse);
      } else {
        if(respuesta != null) {
          console.log("VAL OK");
          if(respuesta.password == pass){
            let session={"logged":true};
            let login = '{"$set":' + JSON.stringify(session) + '}';
            console.log(JSON.parse(login));
            http_client.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab, JSON.parse(login), function(errorP, respuestaMLabP, bodyP) {
              //response.send(bodyP);
              delete respuesta.password;
              delete respuesta._id;
              response.status(200).send(respuesta);
            });
          }else{
            response.status(404).send({"msg":"contraseña incorrecta"});
          }
        } else {
          tempResponse = {"msg" : "Usuario no encontrado."};
          response.status(404);
          response.send(tempResponse);
        }
      }
  });
});

app.post(URL_BASE + 'logout',
  function(request, response) {
    console.log("POST /apicol/v2/login");
    console.log(request.body.email);

    let user = request.body.email;

    let queryStringEmail='q={"email":"' + user + '"}&';

    let http_client = request_json.createClient(URL_mLab);
    http_client.get('user?' + queryStringEmail + apikey_mlab, function(err, rpt, body) {
      console.info(body);
      let tempResponse = {};
      var respuesta = body[0];
      if(err) {
        tempResponse = {"msg" : "Error al recuperar users de mLab."}
        response.status(500);
        response.send(tempResponse);
      } else {
        if(respuesta != null) {
          console.log("VAL OK");

          let session={"logged":false};
          let login = '{"$set":' + JSON.stringify(session) + '}';
          console.log(JSON.parse(login));
          http_client.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab, JSON.parse(login), function(errorP, respuestaMLabP, bodyP) {
            //response.send(bodyP);
            delete respuesta.password;
            delete respuesta._id;
            //response.status(200).send(respuesta);
            response.send({"msg": "logout Exitoso"});
          });
          
        } else {
          tempResponse = {"msg" : "Usuario no encontrado."};
          response.status(404);
          response.send(tempResponse);
        }
      }
  });
});

/*
app.post(URL_BASE + 'logout', function (request, response) {
  console.log("POST /apicol/v2/logout");
  var userId = request.body.id;
  for (us of usersFile) {
    if (us.id == userId) {
      if (us.logged) {
        delete us.logged; // borramos propiedad 'logged'
        writeUserDataToFile(usersFile);
        console.log("Logout correcto!");
        response.status(201).send({ "msg": "Logout correcto.", "idUsuario": us.id });
        return;
      }
      break;
    }
  }
  console.log("Sin Coincidencia.");
  response.status(404).send({ "msg": "Logout incorrecto." });
});

app.get(URL_BASE + 'total_users', function (request, response) {
  console.log("GET users/total_users");
  response.status(200).send({ "num_usuarios": usersFile.length });
});
*/